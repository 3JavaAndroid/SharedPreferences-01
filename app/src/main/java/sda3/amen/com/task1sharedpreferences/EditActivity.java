package sda3.amen.com.task1sharedpreferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditActivity extends AppCompatActivity {
    // prefs name
    public static final String PREFERENCES_PATH = "prefs.db";

    // preferences keys
    public static final String PREFERENCE_NAME = "user_name";
    public static final String PREFERENCE_SURNAME = "user_surname";
    public static final String PREFERENCE_LANG = "user_lang";
    public static final String PREFERENCE_COUNTRY = "user_country";

    @BindView(R.id.editName)
    protected EditText editName;

    @BindView(R.id.editSurname)
    protected EditText editSurname;

    @BindView(R.id.buttonSave)
    protected Button buttonSave;

    @BindView(R.id.buttonCancel)
    protected Button buttonCancel;

    @BindView(R.id.spinnerCountry)
    protected Spinner spinnerCountry;

    @BindView(R.id.spinnerLanguage)
    protected Spinner spinnerLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);

        ArrayAdapter<String> adapterCountries = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.countries));
        spinnerCountry.setAdapter(adapterCountries);

        ArrayAdapter<String> adapterLanguages = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.languages));
        spinnerLanguage.setAdapter(adapterLanguages);

        // Loading preferences at the beginning
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_PATH, MODE_PRIVATE);
        spinnerCountry.setSelection(adapterCountries.getPosition(preferences.getString(PREFERENCE_COUNTRY, MainActivity.DEFAULT_VALUE)));
        spinnerLanguage.setSelection(adapterLanguages.getPosition(preferences.getString(PREFERENCE_LANG, MainActivity.DEFAULT_VALUE)));
        editName.setText(preferences.getString(PREFERENCE_NAME, MainActivity.DEFAULT_VALUE));;
        editSurname.setText(preferences.getString(PREFERENCE_SURNAME, MainActivity.DEFAULT_VALUE));
    }

    @OnClick(R.id.buttonSave)
    protected void savePreferences() {
        if(editName.getText().toString().isEmpty() ||
                editSurname.getText().toString().isEmpty()){
            Toast.makeText(this, "Name or surname is empty.", Toast.LENGTH_SHORT).show();
            return;
        }
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_PATH, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_NAME, editName.getText().toString());
        editor.putString(PREFERENCE_SURNAME, editSurname.getText().toString());
        editor.putString(PREFERENCE_COUNTRY, spinnerCountry.getSelectedItem().toString());
        editor.putString(PREFERENCE_LANG, spinnerLanguage.getSelectedItem().toString());

        if(editor.commit()){
            Toast.makeText(this, "Preferences saved", Toast.LENGTH_SHORT).show();
        }

        // zamknięcie activity
        finish();
    }

    @OnClick(R.id.buttonCancel)
    protected void closeActivity() {
        finish();
    }
}
