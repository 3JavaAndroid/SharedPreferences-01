package sda3.amen.com.task1sharedpreferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public static final String DEFAULT_VALUE = "";
    @BindView(R.id.textName)
    protected TextView textName;

    @BindView(R.id.textSurname)
    protected TextView textSurname;

    @BindView(R.id.textCountry)
    protected TextView textCountry;

    @BindView(R.id.textLanguage)
    protected TextView textLanguage;

    @BindView(R.id.editButton)
    protected Button buttonEdit;

    /**
     * OnClick naszego buttonu - przejście do drugiegi activity.
     */
    @OnClick(R.id.editButton)
    protected void editPreferences(){
        Intent i = new Intent(this, EditActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = getSharedPreferences(EditActivity.PREFERENCES_PATH, MODE_PRIVATE);
        String nameFromPreferences = preferences.getString(EditActivity.PREFERENCE_NAME, DEFAULT_VALUE);
        String surnameFromPreferences = preferences.getString(EditActivity.PREFERENCE_SURNAME, DEFAULT_VALUE);
        String countryFromPreferences = preferences.getString(EditActivity.PREFERENCE_COUNTRY, DEFAULT_VALUE);
        String languageFromPreferences = preferences.getString(EditActivity.PREFERENCE_LANG, DEFAULT_VALUE);

        textName.setText(nameFromPreferences);
        textSurname.setText(surnameFromPreferences);
        textCountry.setText(countryFromPreferences);
        textLanguage.setText(languageFromPreferences);
    }
}
